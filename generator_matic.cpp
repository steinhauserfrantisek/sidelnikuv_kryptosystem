#include <stdio.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include "matice.cpp"

using namespace std;
/*
int kombinacni_cislo2 (int n, int k)
{
  int result = 1;
  for (int i = 0; i < k; ++i)
    {
      result *= (n - i);
      result /= (i + 1);
    }
  return result;
}

int soucet_komb2 (int m, int r)
{
  int result = 0;
  for (int i = 0; i <= r; ++i)
    result += kombinacni_cislo2 (m, i);
  return result;
}

*/
int
main (int argc, char *argv[])
{
  srand (time (NULL));
  if (argc < 4 or argc > 5)
    {
      cerr <<
	"Na vstupu zadejte: jak se ma ulozit verejny klic; parametr r; parametr m; sem muzete ulozit permutacni matici (soukromy klic). Vstupni radek tedy muze byt:\n rm7,3 7 3 perm\n";
      return 1;
    }

  ofstream soubor (argv[1]);
  int m = atoi (argv[2]);
  int r = atoi (argv[3]);

  //generuje RM matici
  RMMatice rm (m, r);

  //generuje nahodnou regularni matici (vytvori nahodnou matici a testuje regularitu - pomale, ale bude nahodna)
  NahodnaMatice *regmatice;
  regmatice = new NahodnaMatice (soucet_komb (m, r), soucet_komb (m, r));

  //  NahodnaMatice regmatice;
  while (!regmatice->test_regularity ())
    {
      delete regmatice;
      regmatice = new NahodnaMatice (soucet_komb (m, r), soucet_komb (m, r));
    }

  //generuje nahodnou permutacni matici
  PermutacniMatice permutacni (1 << m);

  //pokud bylo pet parametru, vypise permutacni matici
  if (argc == 5)
    {
      ofstream permutacnim (argv[4]);
      permutacnim << permutacni;
      permutacnim.close ();
    }

  //vypis do souboru
  soubor << (*regmatice) * rm * permutacni;
  soubor.close ();
  return 0;
}
