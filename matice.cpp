#include "matice.h"
#include <cstdlib>
#include <iostream>

using namespace std;

int
kombinacni_cislo (int n, int k)
{
  int result = 1;
  for (int i = 0; i < k; ++i)
    {
      result *= (n - i);
      result /= (i + 1);
    }
  return result;
}

int
soucet_komb (int m, int r)
{
  int result = 0;
  for (int i = 0; i <= r; ++i)
    result += kombinacni_cislo (m, i);
  return result;
}

BoolMatice::BoolMatice (int sloupce, int radky)
{
  sloupcu = sloupce;
  radku = radky;
  data = new bool[sloupcu * radku];
}

std::ostream & operator<< (std::ostream & os, const BoolMatice & ven)
{
  for (int i = 0; i < ven.radku; ++i)
    {
      for (int j = 0; j < ven.sloupcu; ++j)
	os << ven.data[i * ven.sloupcu + j];
      os << endl;
    }
  return os;
}

BoolMatice & BoolMatice::operator* (const BoolMatice & druha)
{
  if (sloupcu != druha.radku)
    return *(new BoolMatice (0, 0));
  BoolMatice *
    vysledek = new BoolMatice (druha.sloupcu, radku);
  for (int i = 0; i < radku; ++i)
    for (int j = 0; j < druha.sloupcu; ++j)
      {
	bool
	  pom = false;
	for (int k = 0; k < sloupcu; ++k)
	  pom ^= data[i * sloupcu + k] * druha.data[k * druha.sloupcu + j];
	vysledek->data[i * vysledek->sloupcu + j] = pom;
      }
  return *vysledek;
}

BoolMatice::BoolMatice (int sloupce, int radky, std::istream & is)
{
  sloupcu = sloupce;
  radku = radky;
  data = new bool[sloupcu * radku];
  for (int i = 0; i < sloupcu * radku; ++i)
    {
      char c;
      is >> c;
      data[i] = (c != *"0");
    }
}

void
BoolMatice::uprav ()
{
  int zaci = 0;
  int zacj = 0;
  bool prohozen;
  while (zacj < sloupcu)
    {
      prohozen = false;
      for (int i = zaci; i < radku && prohozen == false; ++i)
	{
	  if (data[i * sloupcu + zacj])
	    {
	      bool p;
	      for (int k = zacj; k < sloupcu; ++k)
		{
		  p = data[i * sloupcu + k];
		  data[i * sloupcu + k] = data[zaci * sloupcu + k];
		  data[zaci * sloupcu + k] = p;
		}
	      prohozen = true;
	      ++zaci;
	      ++zacj;
	      break;
	    }
	}
      if (prohozen)
	{
	  for (int i = zaci; i < radku; ++i)
	    if (data[i * sloupcu + zacj - 1])
	      for (int k = zacj - 1; k < sloupcu; ++k)
		data[i * sloupcu + k] ^= data[(zaci - 1) * sloupcu + k];
	}
      else
	++zacj;
    }
}

bool
BoolMatice::test_regularity ()
{
  BoolMatice matice (sloupcu, radku);
  for (int i = 0; i < sloupcu; ++i)
    for (int j = 0; j < radku; ++j)
      matice.data[j * sloupcu + i] = data[j * sloupcu + i];
  matice.uprav ();
  return matice.data[radku * sloupcu - 1];	//resim jen ctvercove matice
}

void
BoolMatice::diagonalizace_na_nahodnych_sloupcich ()
{
  int sloupec = 0;
  int j;
  bool hledej;
  for (int radka = 0; radka < radku; ++radka)
    {
      hledej = true;
      sloupec = rand () % sloupcu;
      while (hledej)
	{
	  for (j = radka; j < radku; ++j)
	    if (data[j * sloupcu + sloupec])
	      {
		hledej = false;
		break;
	      }
	  if (hledej)
	    sloupec = rand () % sloupcu;
	}

      if (j != radka)
	for (int k = 0; k < sloupcu; ++k)
	  {
	    bool pom = data[radka * sloupcu + k];
	    data[radka * sloupcu + k] = data[j * sloupcu + k];
	    data[j * sloupcu + k] = pom;

	  }
      for (int k = 0; k < radka; ++k)
	{
	  if (data[k * sloupcu + sloupec])
	    {
	      for (int l = 0; l < sloupcu; ++l)
		data[k * sloupcu + l] ^= data[radka * sloupcu + l];
	    }

	}
      for (int k = radka + 1; k < radku; ++k)
	{
	  if (data[k * sloupcu + sloupec])
	    {
	      for (int l = 0; l < sloupcu; ++l)
		data[k * sloupcu + l] ^= data[radka * sloupcu + l];
	    }

	}
    }
}


BoolMatice::~BoolMatice ()
{
  sloupcu = 0;
  radku = 0;
  delete[]data;
}


NahodnaMatice::NahodnaMatice (int sloupce, int radky):
BoolMatice (sloupce, radky)
{
  for (int i = 0; i < sloupce * radky; ++i)
    data[i] = rand () % 2;
}

RMMatice::RMMatice (int m, int r):
BoolMatice (1 << m, soucet_komb (m, r))
{
  int p = r;
  if (m < r)
    p = m;
  if (p == 0)
    for (int i = 0; i < (1 << m); ++i)
      data[i] = 1;
  else
    {
      RMMatice g1 (m - 1, p);
      RMMatice g2 (m - 1, p - 1);
      for (int i = 0; i < (1 << (m - 1)); ++i)
	for (int j = 0; j < soucet_komb (m - 1, p); ++j)
	  {
	    data[j * sloupcu + i] = g1.data[j * g1.sloupcu + i];
	    data[j * sloupcu + i + (1 << (m - 1))] =
	      g1.data[j * g1.sloupcu + i];
	  }
      for (int i = 0; i < (1 << (m - 1)); ++i)
	for (int j = 0; j < soucet_komb (m - 1, p - 1); ++j)
	  data[(j + soucet_komb (m - 1, p)) * sloupcu + i] = 0;
      for (int i = (1 << (m - 1)); i < (1 << m); ++i)
	for (int j = 0; j < soucet_komb (m - 1, p - 1); ++j)
	  data[(j + soucet_komb (m - 1, p)) * sloupcu + i] =
	    g2.data[j * g2.sloupcu + i - (1 << (m - 1))];
    }
}

PermutacniMatice::PermutacniMatice (int rozmer):
BoolMatice (rozmer, rozmer)
{
  int per[rozmer];
  int p;
  for (int i = 0; i < rozmer; i++)
    for (int j = 0; j < rozmer; j++)
      data[i * rozmer + j] = 0;
  bool w = 0;
  for (int i = 0; i < rozmer; i++)
    {
      while (w == 0)
	{
	  p = rand () % rozmer;
	  w = 1;
	  for (int j = 0; j < i; j++)
	    if (p == per[j])
	      {
		w = 0;
		break;
	      }
	}
      per[i] = p;
      w = 0;
      data[p * rozmer + i] = 1;
    }
}
