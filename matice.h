#ifndef INCLUDE_MATICE_H
#define INCLUDE_MATICE_H
#include <iostream>

int soucet_komb(int n, int k);

class BoolMatice{
    public:
          BoolMatice(int sloupce, int radky);
          friend std::ostream& operator<<(std::ostream& os, const BoolMatice& ven);
          BoolMatice& operator*(const BoolMatice& druha);
          BoolMatice(int sloupce, int radky, std::istream &is);
          void uprav();
          bool test_regularity();
          void diagonalizace_na_nahodnych_sloupcich();
          ~BoolMatice();
          bool *data;
          int sloupcu;
          int radku;
};

class NahodnaMatice : public BoolMatice{
     public:
           NahodnaMatice(int sloupce, int radky);
};

class RMMatice : public BoolMatice{
     public:
           RMMatice(int m, int r);
};
class PermutacniMatice : public BoolMatice{
     public:
           PermutacniMatice(int m);
};
#endif
