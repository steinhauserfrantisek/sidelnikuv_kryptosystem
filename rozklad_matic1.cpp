#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <math.h>
#include "matice.cpp"

using namespace std;

int kombinacni_cislo2 (int n, int k)
{
    int result = 1;
    for (int i = 0; i < k; ++i)
    {
        result *= (n - i);
        result /= (i + 1);
    }
    return result;
}

int soucet_komb2 (int m, int r)
{
    int result = 0;
    for (int i = 0; i <= r; ++i)
        result += kombinacni_cislo2 (m, i);
    return result;
}


int main (int argc, char *argv[]) {
    if (argc < 4 or argc > 6) {
        fputs("Na vstupu zadejte zezacatku stejny radek jako pri generovani matice,\
 tedy: verejny klic, parametry m a r. A muzete: kam ulozit opravujici matici,\
 pripadne i kam ulozit opraveny a upraveny verejny klic, tedy jiz generujici matici.\
 Pokud posledni dva parametry nezadate, vypisi se na vystup. Typycky vstupni radek\
 tedy muze byt: rm7,3 7 3 operm oprav\n",
              stderr);
        return 1;
    }

    srand((unsigned) time(NULL));
    ifstream in(argv[1]);    //vstupni soubor
    int vstupm = atoi(argv[2]);    //vstupni promena
    int vstupr = atoi(argv[3]);    //vstupni promena (tuhle budu prepisovat)
    int pocet = 200;        //odhadnuty pocet slov minimalni vahy
    int delka = (1 << vstupm);    //delsi strana matice
    int rozmer1 = soucet_komb2(vstupm, vstupr);    //mensi strana matice

    int pocetslov = 0;
    int bylonalezeno = 0;
    //cout << argv[1];
    bool slovo[delka];        //pokazde slovo minimalni vahy, jehoz zbytkovy kod rozklada
    BoolMatice novamatice(delka, rozmer1);
    BoolMatice vypocetni(delka, rozmer1);
    BoolMatice puvodnimatice(delka, rozmer1, in);

    for (int i = 0; i < rozmer1 * delka; i++)
        novamatice.data[i] = puvodnimatice.data[i];    //ulozeni vstupu
    int dvojice[delka][delka];
    //cout << puvodnimatice;
    int hranice; //pokud nemam rozlozeno, je to asi spatne slovo
    int rozmer3;
    bool regularita;
    int x;            //prenosny bit
    while (vstupr > 1) {                //dokud nema matici RM^T(m,1)
//po kazdem snizeni r se nektere promene musi pocitat odznova

        switch (vstupr) {//konstanty jsou odhadnute
            break;
            case 2:
                hranice = 3;
                break;
            case 3:
                switch (vstupm) {
                    case 6:
                        hranice = 40;
                        break;
                    case 7:
                        hranice = 20;
                        break;
                    case 8:
                        hranice = 15;
                        break;
                    case 9:
                        hranice = 10;
                        break;
                    case 10:
                        hranice = 10;
                        break;
                    case 11:
                        hranice=10; //neni overena
                        break;}
            case 4:
                switch (vstupm) {
                    case 8:
                        hranice = 140;
                        break;
                    case 9:
                         hranice = 70;
                         break;
                    case 10:
                         hranice = 70;//neni overena
                         break;
                    case 11:
                         hranice=70;//neni overena
                         break;
                }
              break;
            default:
                hranice = 500;//alespon vypise, ze to zkousel vicekrat
                break;
            }

                        rozmer1 = soucet_komb2(vstupm, vstupr);

                        rozmer3 = soucet_komb2(vstupm, vstupr - 1);
                        int moc;
                        int u = 1;
                        int v = 1;
                        u = rozmer1 + (1 << vstupm - 1);    //misto v pameti, nemelo by preteci - odhad
                        v = (1 << (vstupm - vstupr)) +
                            1;    //je treba mit tento rozmer velky, protoze tam matice bude rust
                        int skupiny[u][v];        //sloupce a rady (rady maji stejna cisla)


                        // u = rozmer1 + (1 << vstupm-1);
                        // v = (1 << (vstupm-2))+1;
                        for (int m = 0; m < u; m++)
                            for (int n = 0; n < v; n++)
                                skupiny[m][n] = -1;    //vymaze skupiny
                        regularita = false;
                        x = -1;
                        for (int i = 0; i < rozmer1 * delka; i++) {
                            vypocetni.data[i] = novamatice.data[i];
                            novamatice.data[i] = false;
                        }
                        vypocetni.radku = rozmer1;
                        novamatice.radku = rozmer1;
                        while (regularita == false) {            //dokud neni matice RM^T(m,r-1) regularni, hledam vektory
                            pocetslov = 0;
                            x++;
                            //napred hleda slovo podle ktereho bude restringovat
                            while (pocetslov == 0) {

                                vypocetni.diagonalizace_na_nahodnych_sloupcich();

                                //hledani slova minimalni vahy
                                for (int i = 0; i < rozmer1; i++) {
                                    u = 0;
                                    for (int j = 0; j < delka; j++) {
                                        if (vypocetni.data[i * vypocetni.sloupcu + j] == 1)
                                            u++;
                                    }        //scita jednicky na radku
                                    if (u == 1 << (vstupm - vstupr)) {
                                        bool tr, fl;
                                        fl = true;
                                        //kdyz je slovo male vahy, tak proveruje, jestli uz podle neho nerestringoval
                                        // nebo nevzniklo rozkladem
                                        for (int y = 0; y < x; y++)
                                            for (int z = 0; z < (1 << vstupr); z++) {
                                                tr = false;
                                                for (int q = 0; q < 1 << (vstupm - vstupr); q++)
                                                    if (!vypocetni.data[i * vypocetni.sloupcu +
                                                                        skupiny[y * (1 << vstupr) + z][q]])
                                                        tr = true;
                                                if (tr == false)
                                                    fl = false;
                                            }
                                        if (fl) {    //jestli se nove slovo od vsech lisilo, tak ho zapise a konci

                                            for (int p = 0; p < delka; ++p) {
                                                slovo[p] = 0;
                                            }
                                            pocetslov++;
                                            for (int w = 0; w < delka; w++)
                                                slovo[w] =
                                                        vypocetni.data[i * vypocetni.sloupcu + w];

                                            break;
                                        }
                                    }
                                }
                            }
                            //ted uz jedno slovo minimalni mam a hledam dalsi, ktere jsou s nim disjunktni
                            pocetslov = 0;
                            for (int j = 0; j < delka; j++)
                                for (int i = 0; i < delka; i++)
                                    dvojice[i][j] = 0;
                            bool mam_skupiny = false;
                            moc = 0;
                            while (!mam_skupiny) {  //cout<<" bezim po "<<moc<<". ";
                                moc++;
                                //cout << "," << flush;
                                while (pocetslov < pocet) {
                                    //dokud nema dostatek slov minimalni vahy restringovaneho kodu hleda dalsi
                                    vypocetni.diagonalizace_na_nahodnych_sloupcich();

                                    //hledani slov malych vah - podobne jako u hledani prvniho slova
                                    //ale proveruje disjunktnost pouze s jednim slovem, hleda vsechna tato slova
                                    //a zapisuje vsechny dvojice jednicek
                                    for (int i = 0; i < rozmer1; i++) {
                                        u = 0;
                                        for (int j = 0; j < delka; j++)
                                            if (vypocetni.data[i * vypocetni.sloupcu + j] == 1)
                                                u++;    //scita jednicky na radku
                                        if (u <= ((1 << (vstupm - vstupr)) + (1 << (vstupm - vstupr -
                                                                                    2))))//ve skutecnosti staci vedet, ze jeho polynom ma stupen r
                                            //if (u <= ((1 << (vstupm - vstupr))) )//o teto variante jsem dokazal, ze funguje
                                        {
                                            bool jev = true;
                                            //kdyz je slovo male vahy, zjisti, jestli je ve zkracenem kode
                                            for (int w = 0; w < delka; w++)
                                                if ((slovo[w]) &
                                                    (vypocetni.data[i * vypocetni.sloupcu + w]))
                                                    jev = false;
                                            if (jev) {
                                                pocetslov++;
                                                //zapisuje dvojice, ktere byli ve slove
                                                int p = 0;
                                                int r[u];
                                                for (int j = 0; j < delka; j++)
                                                    if (vypocetni.data[i * vypocetni.sloupcu + j]) {
                                                        r[p] = j;
                                                        p++;
                                                    }
                                                for (int l = 0; l < p; l++)
                                                    for (int j = 0; j < l; j++)
                                                        dvojice[r[j]][r[l]]++;
                                            }    //konec zapisu dvojic
                                        }
                                    }
                                }            //konec hledani slov malych vah, zjisteni poctu slov

                                //prevod z dvojic do skupin - pouzite dvojice se pritom mazou
                                //cilem je rozdeleni radku do skupin podle toho, jak jsou spolu v podkodech
                                u = rozmer1 + (1 << vstupm - 1);
                                v = (1 << (vstupm - vstupr)) + 1;    //rozmery matice skupiny
                                int f, k, l, pk = -1, pl = -1;    //promene a jejich pomocne
                                int tp = (x << vstupr);    //pocet funkcich radku
                                int t[u];        //kolik je na kazdem radku obsazeno
                                for (int f = tp; f < u; f++)
                                    t[f] = 0;
                                int razeni = ((1 << (vstupm - vstupr)) - 1) *
                                             ((1 << vstupr) - 1);    //kolik dvojic je treba pridat

                                mam_skupiny = true;//predpoklada, ze to vyjde
                                while (razeni > 0) {
                                    int nejvetsi = 0;
                                    for (k = 1; k < delka; k++)
                                        for (l = 0; l < k; l++)
                                            if (dvojice[l][k] > nejvetsi) {
                                                nejvetsi = dvojice[l][k];
                                                pk = k;
                                                pl = l;
                                            }
                                    dvojice[pl][pk] *= -1;
//cout <<dvojice[pl][pk]<<endl;	//dvojici s nejvetsi hodnoutou ulozi a vymaze
                                    k = -1;
                                    l = -1;
                                    for (int m = (x << vstupr); m < u; m++)
                                        for (int n = 0; n < v; n++) {
                                            //najde souradnice cisel z dvojice v jiz utvorenych skupinach
                                            if (skupiny[m][n] == pk)
                                                k = m;
                                            if (skupiny[m][n] == pl)
                                                l = m;
                                        }
                                    if ((-1 == l) and (k == -1)) {        //nenasel ani jedno cislo z dvojice
                                        t[tp] += 2;
                                        skupiny[tp][0] = pl;
                                        skupiny[tp][1] = pk;
                                        tp++;
                                        razeni--;
                                    }        //da oba na novy radek
                                    if ((k == -1) and
                                        (-1 != l)) {        //pokud "l" uz v nejake skupine je ale "k" neni
                                        skupiny[l][t[l]] = pk;
                                        if (t[l] == (1 << (vstupm - vstupr))) {
                                            mam_skupiny = false;
                                            razeni = 1;
                                        } else t[l]++;
                                        razeni--;
                                    }        //da k na stejny radek jako je l
                                    if ((l == -1) and
                                        (-1 != k)) {        //pokud "k" uz v nejake skupine je ale "l" neni
                                        skupiny[k][t[k]] = pl;
                                        if (t[k] == (1 << (vstupm - vstupr))) {
                                            mam_skupiny = false;
                                            razeni = 1;
                                        } else t[k]++;
                                        razeni--;
                                    }        //da l na stejny radek mjako je k
                                    if ((-1 != l) and (k != -1) and
                                        (k != l))    //kdyz uz obe jsou na ruznych radcich, tak radky spojim
                                    {
                                        f = t[k];
                                        if ((t[l] + t[k]) >= (1 << (vstupm - vstupr))) {
                                            mam_skupiny = false;
                                            razeni = 1;
                                        } else
                                            for (int tt = 0; tt < f; tt++) {
                                                skupiny[l][t[l]] = skupiny[k][tt];
                                                skupiny[k][tt] = -1;
                                                t[l]++;
                                            }
                                        t[k] = 0;
                                        razeni--;
                                    }
                                }            //dvojice jsou prevedene do skupin sloupcu

                                //for(int d=0;d<(2<<vstupr)-1;++d){
                                //   for(int c=0;c<(1 << (vstupm-vstupr))+1;++c)cout << skupiny[(x << vstupr)+d][c] << " "; cout << endl<<endl;}
                                //tady radky urovna, aby skupiny byli nahore
                                if (mam_skupiny)
                                    for (int k = (x << vstupr); k < ((x + 1) << vstupr) - 1; k++) {
                                        if (t[k] == 0) {
                                            for (int l = ((x + 1) << vstupr) - 1; l < u; l++)
                                                if (t[l] != 0) {
                                                    for (int qq = 0; qq < t[l]; qq++) {
                                                        skupiny[k][qq] = skupiny[l][qq];
                                                        skupiny[l][qq] = -1;
                                                    }
                                                    t[l] = 0;
                                                    l = u;
                                                }
                                        }
                                    }

/*for(int d=0;d<(1<<vstupm);++d){
   for(int c=0;c<(1<<vstupm);++c)cout << dvojice[d][c] << " "; cout << endl<<endl;}
cout <<"\nkonec dvojic\n";
for (int w = 0; w < delka; w++) cout << slovo[w]; cout<<endl;
for(int d=0;d<(2<<vstupr)-1;++d){
   for(int c=0;c<(1<<vstupm);++c)cout << skupiny[(x << vstupr)+d][c] << " "; cout << endl<<endl;}
*/       for (int d = 0; d < delka; ++d)
                                    for (int c = 0; c < delka; ++c)
                                        if (dvojice[c][d] < 0) { dvojice[c][d] *= -1; }//cout << dvojice[c][d] << endl;}

                                //kontrola, jestli jsou skupiny spravne rozdelene
                                if (mam_skupiny)
                                    for (int r = 0; r < (1 << vstupr) - 1; ++r)
                                        if (skupiny[(x << vstupr) + r][(1 << (vstupm - vstupr)) - 1] == -1) {
                                            mam_skupiny = false;
                                            cout << "jeden krok\n";
                                            break;
                                        }// cout << (1<<(vstupm-vstupr))<<"--" <<r<<" =r  ";}
                                pocetslov = 1;
                                if (!mam_skupiny)
                                    for (int d = 0; d < (rozmer1 + (1 << vstupm - 1) - (x << vstupr)); ++d)
                                        for (int c = 0; c < (1 << vstupm); ++c) skupiny[(x << vstupr) + d][c] = -1;
                                if (moc > hranice)//kdyz neco nevyslo
                                {
                                    mam_skupiny = true;
                                    --x;
                                    //cout << "prelezl jsem\n" << flush;
                                }
                            }//ukoncuje cyklus mam_skupiny
                            //slovo podle krereho jsem restingoval napisu dolu (je to take skupina sloupcu)
                            int l = 0;
                            for (int k = 0; k < delka; k++)
                                if (slovo[k]) {
                                    skupiny[((x + 1) << vstupr) - 1][l] = k;
                                    l++;
                                }
                            //kdyz neco nevyslo
                            if (moc <= hranice) { ;

                                //mam skupiny, ted z nich udelam kodova slova RM^T(m,r-1) a pripisu je do novamatice
                                for (int i = 0; i < (1 << vstupr) - 1; i++) {
                                    for (int j = 0; j < 1 << (vstupm - vstupr); j++) {
                                        novamatice.data[(bylonalezeno + i + 1) * novamatice.sloupcu +
                                                        skupiny[i + (x << vstupr)][j]] = true;
                                        novamatice.data[(bylonalezeno + i + 1) * novamatice.sloupcu +
                                                        skupiny[((x + 1) << vstupr) - 1][j]] = true;
                                    }
                                }

                                //uprava matice aby byla videt hodnost (horni trojuhelnik)
                                novamatice.uprav();

                                //do bylonalezeno ulozi hodnost matice a pripadne zacne hledat dalsi odznova
                                bylonalezeno = 0;
                                for (int j = 0; j < delka; j++)
                                    for (int i = 0; i < rozmer3; i++)
                                        if (novamatice.data[i * novamatice.sloupcu + j])
                                            if (i > bylonalezeno)
                                                bylonalezeno = i;
                                //printf("zatim je: %d je treba %d bylo treba %d kroku\n", bylonalezeno + 1, rozmer3 moc);
                                if (bylonalezeno + 1 >= rozmer3)
                                    regularita = true;
                            }
                        }
                        //konec jednoho cyklu na snizeni hodnoty r, zmenim parametry a jdu znovu
                        bylonalezeno = 0;
                        vstupr--;
                        //printf ("mam matici RM^P(%d,%d)\n", vstupm, vstupr);
                }                //konec vsech cyklu snizovani r
//ted uz ma matici RM^P(m,1) odvozuje permutacni matici

//ze vsech souctu odstrani jednotkove slovo
                for (int i = 0; i < rozmer1; i++)
                    if (novamatice.data[i << vstupm] == 1)
                        for (int j = 0; j < delka; j++)
                            novamatice.data[(i << vstupm) + j] ^= 1;

//seradi do castecne horni trojuhelnikove matice, tim zaroven snizi hodnost,
//protoze je odstranena 1. Take bude permutace co nejblizsti identite
                novamatice.uprav();

//prevadi sloupce do jejich bitoveho zapisu a vytvari permutacni matici
                PermutacniMatice permut(delka);
                for (int i = 0; i < delka * delka; i++)
                    permut.data[i] = 0;
                int s;
                for (int i = 0; i < delka; i++) {
                    s = 0;
                    for (int j = 0; j < rozmer3 - 1; j++) {
                        if (novamatice.data[j * novamatice.sloupcu + i])
                            s += (1 << j);
                    }
                    permut.data[i * permut.sloupcu + s] = 1;
                }

//vypisuje opravujici permutacni matici
/*  if (argc >= 5)
    {
      ofstream soubor (argv[4]);
      soubor << permut;
    }
  else
    cout << permut;
*/
//permutuje sloupce
                novamatice = puvodnimatice * permut;

//uprava do horni trojuhelnikove matice, kde bude alespon trochu videt charakterm matice
                novamatice.uprav();
//varianty vypisu opravene matice
                if (argc == 5) {
                    ofstream vypis(argv[4]);
                    vypis << novamatice;
                } else
                    cout << novamatice;
                return (0);
        }
//    }
//}
